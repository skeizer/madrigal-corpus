![Creative Commons License](https://i.creativecommons.org/l/by-sa/4.0/88x31.png)

This work is licensed under a [Creative Commons Attribution-ShareAlike 4.0 International License](http://creativecommons.org/licenses/by-sa/4.0/).

# madrigal-data

Human evaluation data collected in the Madrigal project.  Please cite the following paper:

* Simon Keizer, Ondrej Dušek, Xingkun Liu, and Verena Rieser.  *User Evaluation of a Multi-dimensional Statistical Dialogue System*.
Proc. SIGdial, September 2019.
[arXiv:1909.02965 [cs.CL cs.AI]](https://arxiv.org/abs/1909.02965)


## Full dialogue logs

The data are contained in the file `call_data.json`. It contains a list of dictionaries, one for each dialogue/call. The main structure of each call dictionary is the following:

- **dialogueID**: unique dialogue ID
- **userID**: unique crowd worker ID
- **sysName**: system variant used for this dialogue; the names correspond to the four variants from the paper as follows:
    - **sysAy**: multi-dim (y = 1 or 2)
    - **sysBy**: one-dim
    - **sysCy**: trans-fixed
    - **sysDy**: trans-adapt
    - **sysX1**: ASR hypotheses shown on the web interface (X = A , B, C, or D)
    - **sysX2**: no ASR hypotheses shown on the web interface
    
- **audio_dir**: path to the audio recordings (see below)
- **CF_data**: crowdsourcing data, including worker information
	- **task_desc**: task description shown to the crowd worker
	- **q1,q2,...q6**: responses to the questionnaire
- **dialogue**: the individual dialogue turns (list of dictionaries, each containing the *usr_turn* and *sys_turn* entries)
    - **usr_turn**: user input
        - **asr_hyps**: top ASR hypothesis (the recognized utterance and confidence score) 
        - **asr_full_hyps**: all ASR hypotheses
        - **asr_hyps**: ASR hypotheses (the recognized utterance and confidence score)
        - **top_hyp**: ASR hypothesis with highest confidence score
        - **sem_hyps**: NLU output (recognized dialogue act and confidence score)
        - **transcription**: human transcription of the user utterance (for calls that were manually transcribed)
        - **audio_file**: path of the audio file for the specific turn (for calls that were manually transcribed)
        - **transcription_sem**: NLU output over human transcriptions (for calls that were manually transcribed)
    - **sys_turn**: system response
        - **sys_act**: system response dialogue act
        - **utterance**: system response utterance


## Audio recordings

Audio recordings, divided by user ID and dialogue ID (corresponding to the logs information) are available 
[in the downloads section](https://bitbucket.org/skeizer/madrigal-data/downloads/madrigal-audio.tar.gz) (164MB tar.gz, not versioned).
The audio is in the [OPUS](https://en.wikipedia.org/wiki/Opus_(audio_format)) format, as recorded by Chrome (16-bit, 48 kHZ, lossy compression).
Note that transcriptions for about 1/2 of the recordings are contained in the dialogue logs JSON file.